#!/usr/bin/env node

const prom = require('util').promisify;
const fs = require('fs');
const fsStat = prom(fs.stat);
const fsMkdir = prom(fs.mkdir);
const fsReadFile = prom(fs.readFile);
const fsWriteFile = prom(fs.writeFile);
const fsRename = prom(fs.rename);
const path = require('path');
const SVGO = require('svgo');
const moment = require('moment');

// const filepath = path.resolve(__dirname, 'test.svg');
const svgPaths = [];
let ignoringProcessedFiles = false;

process.argv.forEach((val, index) => {
  if (val.match(/-h|--help/)) {
    logMessage('TODO: print basic help and exit');
    process.exit(0);
  }

  const info = path.parse(val);

  if (val.match(/\.svg$/)) {
    if (val.match(/\.min/)) {
      ignoringProcessedFiles = true;
      logMessage(`skipping processed file "${info.name}${info.ext}"`);
    } else {
      svgPaths.push(path.resolve(val));
    }
  }
});

const svgo = new SVGO({
  plugins: [
    { removeViewBox: false },
    { removeDimensions: true }
  ]
});

if (svgPaths.length === 0) {
  logMessage('error: no valid files provided for processing');
  process.exit(1);
}

if (ignoringProcessedFiles) {
  logMessage(`note: to re-process a file, first remove ".min" from it's name.`);
}

svgPaths.forEach(async (svgPath) => {
  const info = path.parse(svgPath);
  logMessage(`processing SVG "${info.name}${info.ext}"..`);

  try {
    // here we fetch the contents of the input file
    const fileContents = await fsReadFile(svgPath, 'utf8');

    // here we push the contents through SVGO to apply our custom rules
    const optimisedContents = await svgo.optimize(fileContents);

    // here we write the SVG content exposed at "optimised.data" to a new SVG file
    const outputFile = `${info.dir}/${info.name}.min${info.ext}`;
    const outputData = new Uint8Array(Buffer.from(optimisedContents.data));

    await fsWriteFile(outputFile, outputData);
    logMessage(`optimised SVG saved as "${info.name}.min${info.ext}`);

    // here we move the processed file into a backup folder to get it out of the way
    const backupPath = `${info.dir}/backup`;
    try {
      await fsStat(backupPath);
    } catch (error) {
      logMessage('error checking backup path:', error.message || error);
      await fsMkdir(backupPath);
    }
    await fsRename(svgPath, `${backupPath}/${info.name}${info.ext}`);
  } catch (error) {
    throw Error(error.message || error);
  }
});

function logMessage(message) {
  console.log(`[${moment().format('HH:mm:ss')}] ${message}`);
}
