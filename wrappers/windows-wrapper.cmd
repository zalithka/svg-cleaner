echo off
cls

echo.------------------------
echo.  SVG Cleaner CLI Tool
echo.------------------------

:check_node
call npm -v >nul 2>&1
if %ERRORLEVEL% neq 0 goto:missing_npm


:check_cleaner
call svg-cleaner -h >nul 2>&1
if %ERRORLEVEL% neq 0 goto:missing_cleaner


:process_files
echo.
echo.processing files now..
echo.
echo.==  script output  =============================================================
call svg-cleaner %%*
echo.================================================================================

if %ERRORLEVEL% neq 0 goto:unknown_error
goto:cleanup


:missing_npm
echo.
echo.please install NodeJS first and run this script again
goto:cleanup


:missing_cleaner
echo.
echo."svg-cleaner" tool not found, trying to install now..
call npm install -g gitlab:zalithka/svg-cleaner
if %ERRORLEVEL% neq 0 goto:missing_cleaner_failed
goto:check_cleaner


:missing_cleaner_failed
echo.
echo.looks like the "svg-cleaner" installation failed, please check that NodeJS is configured correctly and try again.
goto:cleanup


:unknown_error
echo.
echo.looks like something didn't work, if you saw errors, please resolve them and try again.
goto:cleanup


:cleanup
echo.
pause
goto:eof
