# SVG Cleaner CLI Tool

## Project Goals

a simple NPM CLI tool to script a general "clean up" process for SVG files.

## Installation and Usage

please ensure you have NodeJS version 8 or higher installed first.

install it globally with `npm install --global gitlab:zalithka/svg-cleaner`.

use it from any directory with `svg-cleaner <path/to/svg>`. multiple files may be passed at the same time.

## Platform-Specific Wrappers

this repository contains a [`wrappers` folder](wrappers) with script files, which will install this CLI tool automatically first if it's not already installed. please note that these scripts will also require NodeJS to be installed first.to

use any of them, browse to the file in this GitLab repository viewer, click the "Raw" button on the right-hand side of the file's header, and save the page as a script file. it can be anywhere in your system, although to make things easier, place it inside a folder that you plan to save SVG files most often *before* cleaning. then you can simply drag-and-drop any number of SVG files onto the script to process them.

# Future Plans

* add wrapper scripts for other platforms (Linux, Mac)
* extend wrapper scripts to install NodeJS if required (maybe?)
* extend CLI tool to provide more granular command structure and feedback
* make wrapper scripts pass configuration from a local file (this would make it more portable, allowing custom configuration for different folders)
